TEXFILES := $(shell find src/ -print)
TIKZS = $(patsubst src/figures/dot/%.dot, src/figures/auto-tikz/%.tex, $(wildcard src/figures/dot/*.dot))

whitepaper.pdf: $(TEXFILES)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode whitepaper.tex; \
	rm -f *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib

bib: figures $(TEXFILES)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode whitepaper.tex; \
	bibtex whitepaper.aux; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode whitepaper.tex; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode whitepaper.tex; \
	rm -f *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib; \

figures: $(TIKZS)

src/figures/manual-tikz/*:

src/figures/auto-tikz/%.tex: src/figures/dot/%.dot
	mkdir -p src/figures/auto-tikz/
	dot2tex --texmode math --format tikz --figonly --autosize --usepdflatex --nominsize --prog dot $< > $@

clean:
	rm -f *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib *.pdf
