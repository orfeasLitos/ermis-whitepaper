\section{Smart Contract Details}
\label{sec:details}
Consider a phygital $p$ (conforming to
ERC-721\footnote{\url{https://ethereum.org/en/developers/docs/standards/tokens/erc-721/}}
or
ERC-1155\footnote{\url{https://ethereum.org/en/developers/docs/standards/tokens/erc-1155/}})
owned on-chain by redeemer \alice and the corresponding physical item
$g$ (be it a piece of art, a consumer good, a collectible, etc.) held by provider \bob. The goal of
the protocol is \emph{phygital redemption}, i.e., that \alice burns $p$ on-chain
and \bob ships $g$ to her physical address and gives her a
matching soul-bound token in exchange.

At the heart of this novel protocol is a set of Solidity
Smart
Contracts\footnote{\url{https://github.com/flrfinance/phygital-redemptions-contracts}} % TODO: open-source and link to repo instead
developed in collaboration with Common
Prefix\footnote{\url{https://flrfinance.medium.com/introducing-fflabs-cfa8d580441a}}. The main contract is called \texttt{Phy\-gi\-tal\-Re\-dee\-mer721} or
\texttt{PhygitalRedeemer1155} depending on the type of the
phygital and inherits from \texttt{PhygitalRedeemerCommon}. We
call it \texttt{PR} from now on. Each phygital corresponds to
exactly one \texttt{PR}, which is created by Ēnosys via a
call to the \texttt{createRedeemer()} method of its long-lived
\texttt{PhygitalRedeemerFactory} contract when the phygital is
first created. Upon the creation of \texttt{PR}, the aforementioned countdown, a.k.a. ``maximum
fulfillment duration'' (which prevents the Provider from delaying
fulfillment indefinitely) is also set. It cannot be changed later.
A state machine of \texttt{PR} can be seen in Figure~\ref{fig:state-machine}.

\begin{figure}
\centering
\subimport{./figures/manual-tikz/}{state-machine}
\caption{State Machine for Phygital Redemption Request}
\label{fig:state-machine}
\end{figure}

To initiate redemption, \alice calls \texttt{PR.openRequest()},
specifying her public key, the ID of $p$ and her encrypted
shipping address. A \texttt{requestID} is returned. As an
optimization, a single \texttt{PR.openRequest()} can be used to
request multiple phygitals to be redeemed at once using the exact
same protocol, but we will here focus on a single phygital for
simplicity.

After the request is opened there are three possibilities:
\begin{itemize}
  \item \alice changes her mind and calls \texttt{PR.closeRequest()} with the aforementioned \texttt{requestID},
  \item \bob declines the request by calling
  \texttt{PR.rejectRequest()} with the \texttt{requestID} and a
  free-form reason for rejecting. Possible reasons are, e.g., an
  invalid shipping address,
  \item \bob accepts the request by calling
  \texttt{PR.acceptRequest()} with the \texttt{requestID}.
\end{itemize}
In the first two cases, the contract returns to its initial state
and $p$ returns to \alice. In the third case, $p$ is held by the
contract and a countdown equal to the maximum fulfillment duration starts.

Within that time, \bob has to ship the physical item and call
\texttt{PR.ful\-fill\-Re\-quest()} with the \texttt{requestID} and a
free-form field containing encrypted delivery tracking
information. This function stops the countdown, burns the phygital
and creates a soul-bound ERC-721 token owned by \alice.

If the countdown runs out before fulfillment, \alice can then
cause expiry by calling \texttt{PR.expireRequest()} with
\texttt{requestID}. In this case the contract returns once again
to its initial state and $p$ returns to \alice. If \alice does not
expire the request, it is still possible for \bob to fulfill, even
after the countdown is over.

\alice can decrypt and use the tracking information to monitor the
progress of the physical item delivery, which should eventually
arrive. The protocol is now complete.

In case \bob calls \texttt{PR.fulfillRequest()} but does not
actually ship the physical item, \alice should open a dispute with
Ēnosys. The exact procedure remains as of now unspecified. %TODO: update when procedure is decided

\subsection{Alternative design with one transaction per party}
\label{subsec:2tx}
One of the design choices of Ermis is that the provider submits
two transactions: one for accepting and one for fulfilling a
request. This choice is made for a number of user
experience-enhancing reasons: Firstly, it ensures that the
countdown starts only after the provider first becomes aware of
the request, thus avoiding a situation in which the request is
opened, the countdown almost completes and only then does the
provider check the contract -- this can lead to the provider
rushing to fulfill and worsens their user experience. Secondly,
the redeemer gets more fine-grained feedback on the progress of
their request. Lastly, this design ensures that all communication
happens on-chain, which both simplifies the user experience and
increases accountability.

The drawback of this design is increased on-chain costs. To avoid
this, \texttt{ac\-cept\-Re\-quest} and \texttt{fulfillRequest} can
be combined in one message. In that case, the countdown must start
when the request is opened (and thus the default maximum fulfillment duration
should be higher) and the physical item should only be shipped
after \texttt{fulfillRequest} is finalized (since it is in a race
with a possible \texttt{closeRequest} by the redeemer). Since
tracking information is generated only after shipping begins, it
has to be sent to the redeemer out-of-band. To achieve this, the
redeemer can include an encrypted social media handle in its call
to \texttt{openRequest}. This alternative can be easily
implemented if popular. If the out-of-band communication is
non-repudiable and usable by arbitrators in case of dispute, this
alternative design provides exactly the same guarantees as the
original.
