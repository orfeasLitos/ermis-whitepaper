\section{High Level Overview of Ermis}
\subsection{Before the Protocol}
To set the stage for the Ermis protocol, two things must have happened ahead of
time. First, the Provider (e.g., the artist) has to have
performed KYC with Ēnosys and furnished proof that they have and can deliver the physical
item. A picture or video of the item is sufficient. This is needed to prevent fraudulent or illegal usage, as delivery of the
physical item cannot be enforced on-chain. The Provider must trust that Ēnosys
will carefully guard its private data and only disclose it in case of
fraud by the Provider. Initially, becoming such a provider will only be available via
submission to the Ēnosys team, but the plan is to eventually open Ermis to
external vendors.

Second, the Provider must have sold the phygital. Since they are NFTs, phygitals
are tradeable on-chain exactly like any other NFT. As it is a well-established
kind of transaction and independent of Ermis, we will not go into detail on how
a phygital can be sold.

As a result, a potential buyer that wants to own the physical item without
having it at hand (e.g., an art dealer) will be content with having just the
phygital. The phygital may change many hands many times until it is bought by
someone that desires the corresponding physical item (e.g., a fan of the artist).

\subsection{Honest Protocol Flow}
\begin{figure}
\includegraphics[width=\textwidth]{src/protocol.png}
\caption{Ermis Protocol}
\label{fig:protocol}
\end{figure}

We will now go over the protocol flow when everything works as intended (see
Figure~\ref{fig:protocol}). All communication happens via on-chain transactions.
The relevant messages are emphasized. First, the phygital Redeemer \emph{opens} a
request to redeem the phygital, including its postal address in the request,
encrypted for the Provider. Then the Provider decrypts and checks the address. If it
is valid, they \emph{accept} the request. At that point a countdown starts,
within which the Provider has to \emph{fulfill} the request. Indeed, the Provider
ships the physical item to the Redeemer's address, and does four actions atomically: informs the Redeemer that the
request was fulfilled, provides encrypted tracking information,
burns the phygital and creates a Soul-bound
Token~\cite{weyl2022decentralized} under the possession of the Redeemer. Eventually
the item is delivered to its Redeemer. With this straightforward protocol, the
redemption is successful. No interaction with Ēnosys whatsoever has been
necessary.

\subsection{Benign Failure Scenarios}
Let us now examine the various ways in which redemption can fail. We will first
discuss mishaps after a redemption request is placed, but before it is accepted.
If the Provider is unable to ship or in case the postal address in the redemption
request is malformed, the Provider cannot fulfill and thus has to \emph{reject}
the request. The phygital returns to the Redeemer.

It is also possible that the Provider does not accept for a long time or that the
Redeemer changes its mind soon after placing the request. In either case and as
long as the Provider has not yet accepted, the Redeemer can \emph{close} the request.
Once again, the phygital returns to the Redeemer.

Now we will focus on the case of a successful request and accept, but failure to
fulfill. In this scenario, the countdown (which had started when the Provider accepted) will eventually reach zero. At that
point the Redeemer can \emph{expire} the request. Just like in the two previous
cases, the phygital returns to the Redeemer.

In all three previous failure scenarios no harm has been done, except possibly
for some wasted transaction fees. Indeed both parties are back at their initial
state: the Redeemer has the phygital and the Provider has the physical item.

\subsection{Disputes}
\label{subsec:disputes}
Unfortunately, there is another, arguably much more problematic, failure
scenario: The Provider sends a fulfill transaction without actually shipping the
physical item, thus defrauding the Redeemer by burning the phygital while keeping the
physical item.

Since the blockchain is confined to the virtual realm, it is not
straightforward, and likely impossible, to enforce delivery or revert the
destruction of the phygital in such a case without the help of a trusted third
party. This is where the initial KYC by Ēnosys becomes relevant: In case of
a dispute, either party can raise the issue to Ēnosys and pursue
adjudication with their aid or even via traditional legal avenues.
Since all communication is visible on-chain and all messages are
signed, the entire transcript can be provided to Ēnosys with
guaranteed authenticity. The exact
dispute resolution mechanism will be covered in later revisions of this
document. % TODO: update this when decided
Both the Redeemer and the
Provider have to trust that, as long as they behave honestly throughout the Ermis
protocol and the adjudication process, the dispute will be resolved in their
favor by Ēnosys.

\subsection{Trust Assumptions}
We here reiterate the ways in which the two parties have to trust Ēnosys
for clarity.
\begin{itemize}
\item The \emph{only} way in which the Redeemer has to trust Ēnosys is to
resolve any disputes to their favor as long as the Redeemer behaves honestly.
\item The Provider has to trust Ēnosys in two ways:
  \begin{itemize}
  \item Like the Redeemer, the Provider has to trust that, as long as it is honest
  throughout the protocol and fully cooperative with any dispute adjudication
  process, Ēnosys will resolve all disputes to its favor.
  \item The Provider must trust that Ēnosys will manage the data gathered at
  KYC with the required care: As long as the Provider is honest, their private
  data will be stored securely by Ēnosys, only shared with the minimum number of parties
  needed and thoroughly deleted once they are not useful anymore.
  \end{itemize}
\end{itemize}
The reputation that Ēnosys has accumulated in the blockchain and web3 space
indeed justifies bestowing this minimal level of trust.
